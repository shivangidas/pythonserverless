# pythonserverless

Horizontal parallelization using lambda functions. Function as a Service.

## AWS CLI

1. Get AWS CLI
   <pre>pip3 install awscli --upgrade --user</pre>
2. Add to path
   <pre>export PATH=~/.local/bin:$PATH</pre>
   Check with <pre>aws --version
   > aws-cli/1.16.283 Python/3.7.4 Darwin/18.7.0 botocore/1.13.19</pre>
3. aws configure

   open and edit credentials

   <pre>open ~/.aws/credentials</pre>

   copy the credentials from Account Details

## Serverless

<pre>
npm install -g --save serverless
serverless create -t aws-python3
</pre>

serverless can also be used as sls

## Install boto framework

<pre>pip install boto3</pre>

## Run code

### Deploy on AWS

<pre>sls deploy</pre>

open the get url ending with writeToSQS. The difficulty, time and lambda can added as query parameters

Example: https://7kh7m4kr78.execute-api.us-east-1.amazonaws.com/dev/writeToSQS?difficulty=6&lambda=100&time=20

### remove s3, sqs, and lambdas and all other resources

<pre>serverless remove</pre>

### invoke functions locally

sls invoke local -f function --data '{}'

Architecture

![Architechture](https://gitlab.com/shivangidas/pythonserverless/raw/master/lambda.png)
