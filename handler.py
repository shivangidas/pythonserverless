import hashlib
import sys
import json
import time
import boto3

lambda_client = boto3.client('lambda', region_name="us-east-1",)


def nonce(event, context):
    try:
        # print(event["Records"])
        try:
            inputs = event["Records"][0]["messageAttributes"]
        except Exception as e:
            errorResponse = {
                "errorType": "Exception",
                "errorMessage": str(e)
            }
            return errorResponse

        nonce = 0
        data = "COMSM0010cloud"
        difficulty = 2
        end = 20000
        nonce = int(inputs["nonce"]["stringValue"])
        data = inputs["data"]["stringValue"]
        difficulty = int(inputs["difficulty"]["stringValue"])
        end = int(inputs["end"]["stringValue"])
        startTime = time.time()
        # call function for mining

        gn = goldenNonce(nonce, data, difficulty, end)
        endTime = time.time()
        timeTaken = endTime - startTime

        if gn != -1:
            z = data + str(gn)
            hashValue = hashlib.sha256(hashlib.sha256(
                z.encode('utf-8')).hexdigest().encode('utf-8')).hexdigest()

            # write to output queue
            # Create SQS client
            sqs = boto3.client('sqs')

            # Get URL for SQS queue
            queueURL = sqs.get_queue_url(QueueName='OutputQueue')
            body = {
                "message": "Success",
                "goldenNonce": gn,
                "time": timeTaken,
                "hash": hashValue,
                "difficulty": difficulty
            }
            # Send message to SQS queue
            response = sqs.send_message(
                QueueUrl=queueURL['QueueUrl'],
                DelaySeconds=10,
                MessageAttributes={
                },
                MessageBody=(
                    json.dumps(body)
                )
            )
        responseBody = {"message": "done"}
        successResponse = {
            "statusCode": 200,
            "body": json.dumps(responseBody)
        }
        return successResponse
        # add 500 response too
    except Exception as e:
        errorResponse = {
            "errorType": "Exception",
            "errorMessage": str(e)
        }
        return errorResponse


def goldenNonce(nonce, data, difficulty, end):
    # function that calculates golden nonce
    prefix = "0" * difficulty
    flag = 0
    while nonce <= end:
        z = data + str(nonce)
        hashValue = hashlib.sha256(hashlib.sha256(
            z.encode('utf-8')).hexdigest().encode('utf-8')).hexdigest()
        if hashValue[:difficulty] == prefix:
            flag = 1
            break
        nonce += 1
    if (flag == 0):
        return -1
    else:
        return nonce
