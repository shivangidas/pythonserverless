import json
import time
import boto3


def writeToSQS(event, context):
    try:
        data = "COMSM0010cloud"
        data = ''.join(format(ord(x), 'b') for x in data)
        start_time = time.time()
        max_difficulty = 10
        difficulty = 4
        timeGivenByUser = 60 * 5
        numberOfTasksInParallel = 10
        total_nonce_options = pow(2, 64)
        hashes_per_task = 100000  # computer can make 157011 hashes/second
        body = {}

        # Read and Check Inputs
        try:
            difficulty = int(event["queryStringParameters"]["difficulty"])
        except:
            print(f"No difficulty given. Proceeding with {difficulty}")
        if (difficulty > max_difficulty):
            print(
                f"Difficulty {difficulty} is too much for my capacity. Exiting...")
            body = {
                "message": "Difficulty is too much for my capacity. Exiting.."
            }
            successResponse = {
                "statusCode": 200,
                "body": json.dumps(body)
            }
            return successResponse
            exit(0)
        try:
            numberOfTasksInParallel = int(
                event["queryStringParameters"]["lambda"])
            if numberOfTasksInParallel > 1000:
                print(f"Limited to 1000 lambdas. Proceeding with 1000")
        except:
            print(
                f"Number of simultaneous lambda functions to run not mentioned. Proceeding with {numberOfTasksInParallel}")
        try:
            timeGivenByUser = int(event["queryStringParameters"]["time"])
        except:
            print(
                f"Time not mentioned.Proceeding with {timeGivenByUser} seconds")
        print(
            f'Difficulty: {difficulty} | Max duration: {timeGivenByUser} sec | Lambdas : {numberOfTasksInParallel}\n')
        flag = 0
        endNonceValue = 0
        purgeQueues()  # before begining remove existing data if any
        # Create SQS client

        sqs = boto3.client('sqs')

        # Get URL for SQS queue
        queueURL = sqs.get_queue_url(QueueName='InputQueue')
        outputQueueURL = sqs.get_queue_url(QueueName='OutputQueue')
        # print(queueURL['QueueUrl'])
        while (endNonceValue < total_nonce_options and (time.time() - start_time) < int(timeGivenByUser)):
            startNonce = endNonceValue
            # round_no += 1
            # print(f"Round {round_no} starting at : {startNonce}")
            for i in range(0, numberOfTasksInParallel):
                startNonceValue = startNonce + i * hashes_per_task
                endNonceValue = startNonce + i * hashes_per_task + hashes_per_task
                # print(f"Task {i} starting at {startNonceValue}")
                response = sqs.send_message(
                    QueueUrl=queueURL['QueueUrl'],
                    DelaySeconds=10,
                    MessageAttributes={
                        "nonce": {
                            "DataType": "Number",
                            "StringValue": str(startNonceValue)
                        },
                        "data": {
                            "DataType": "String",
                            "StringValue": str(data)
                        },
                        "difficulty": {
                            "DataType": "Number",
                            "StringValue": str(difficulty)
                        },
                        "end": {
                            "DataType": "Number",
                            "StringValue": str(endNonceValue)
                        }
                    },
                    MessageBody=(
                        'Please get me a nonce'
                    )
                )
                # check for results if any
                if difficulty <= 5:
                    try:
                        response = sqs.receive_message(
                            QueueUrl=outputQueueURL['QueueUrl'],
                            MaxNumberOfMessages=1,
                            VisibilityTimeout=10,
                            WaitTimeSeconds=20,
                            ReceiveRequestAttemptId='string'
                        )
                        message = response['Messages'][0]
                        receipt_handle = message['ReceiptHandle']

                        body = message["Body"]
                        flag = 1
                        # clean up here
                        purgeQueues()

                        successResponse = {
                            "statusCode": 200,
                            "body": json.dumps(body)
                        }
                        return successResponse
                        exit(0)
                        # break
                    except Exception as e:
                        continue
            numberOfTasks = sqs.get_queue_attributes(
                QueueUrl=queueURL['QueueUrl'],
                AttributeNames=[
                    'ApproximateNumberOfMessages', 'ApproximateNumberOfMessagesNotVisible'
                ]
            )
            # print(numberOfTasks)
            numOfTasksInQueue = int(numberOfTasks["Attributes"]["ApproximateNumberOfMessages"]) + int(
                numberOfTasks["Attributes"]["ApproximateNumberOfMessagesNotVisible"])
            # print(numOfTasksInQueue)
            while numOfTasksInQueue != 0:
                try:
                    response = sqs.receive_message(
                        QueueUrl=outputQueueURL['QueueUrl'],
                        MaxNumberOfMessages=1,
                        VisibilityTimeout=10,
                        WaitTimeSeconds=20,
                        ReceiveRequestAttemptId='string'
                    )
                    message = response['Messages'][0]
                    receipt_handle = message['ReceiptHandle']

                    body = message["Body"]
                    flag = 1
                    # clean up here
                    purgeQueues()

                    successResponse = {
                        "statusCode": 200,
                        "body": json.dumps(body)
                    }
                    return successResponse
                    exit(0)
                    # break
                except Exception as e:
                    # do nothing
                    # print(str(e))
                    continue
            if flag == 1:
                break
            if flag == 0:
                # check once more before proceeding
                try:
                    response = sqs.receive_message(
                        QueueUrl=outputQueueURL['QueueUrl'],
                        MaxNumberOfMessages=1,
                        VisibilityTimeout=10,
                        WaitTimeSeconds=20,
                        ReceiveRequestAttemptId='string'
                    )
                    message = response['Messages'][0]
                    receipt_handle = message['ReceiptHandle']

                    body = message["Body"]
                    flag = 1
                    purgeQueues()
                    successResponse = {
                        "statusCode": 200,
                        "body": json.dumps(body)
                    }
                    return successResponse
                    exit(0)
                    # maybe clean up here itself
                    #print('Received and deleted message: %s' % message)
                except Exception as e:
                    # do nothing
                    print("next check", str(e))

            print("Completed one whole round")
        if flag == 1:
            # body = json.loads(body)
            # body["total time"] = time.time() - start_time
            purgeQueues()
            successResponse = {
                "statusCode": 200,
                "body": json.dumps(body)
            }
            return successResponse
            exit(0)
        if ((time.time() - start_time) > int(timeGivenByUser)):
            body = {"message": "Ran out of time"}
        if (endNonceValue > total_nonce_options):
            body = {"message": "Ran out of nonces"}
        purgeQueues()
        successResponse = {
            "statusCode": 200,
            "body": json.dumps(body)
        }
        return successResponse
    except Exception as e:
        #print("we got an error")
        errorResponse = {
            "errorType": "Exception",
            "errorMessage": str(e)
        }
        return errorResponse


def purgeQueues():
    try:
        sqs = boto3.client('sqs')

        # Get URL for SQS queue
        queueURL = sqs.get_queue_url(QueueName='InputQueue')
        outputQueueURL = sqs.get_queue_url(QueueName='OutputQueue')

        response = sqs.purge_queue(
            QueueUrl=outputQueueURL['QueueUrl']
        )
        # purge message from input queue
        response = sqs.purge_queue(
            QueueUrl=queueURL['QueueUrl']
        )
        return
    except Exception as e:
        return
