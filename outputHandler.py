import boto3
import json


def finalOutput(event, context):
    ''' Clean up all tasks
    '''
    try:
        # Create SQS client
        sqs = boto3.client('sqs')

        # Get URL for SQS queue
        queueURL = sqs.get_queue_url(QueueName='InputQueue')
        # purge message from input queue
        response = sqs.purge_queue(
            QueueUrl=queueURL['QueueUrl']
        )

        htmlqueueURL = sqs.get_queue_url(QueueName='HTMLQueue')
        response1 = sqs.send_message(
            QueueUrl=htmlqueueURL['QueueUrl'],
            DelaySeconds=10,
            MessageAttributes={
            },
            MessageBody=(
                json.dumps(event["Records"][0]["body"])
            )
        )
        successResponse = {
            "statusCode": 200,
            "body": json.dumps(event)
        }
        return successResponse
    except Exception as e:
        errorResponse = {
            "errorType": "Exception",
            "errorMessage": str(e)
        }
        return errorResponse


def outputHTML(event, context):
    try:
        # Create SQS client
        sqs = boto3.client('sqs')
        # Get URL for SQS queue
        queueURL = sqs.get_queue_url(QueueName='InputQueue')
        # purge message from input queue
        response = sqs.purge_queue(
            QueueUrl=queueURL['QueueUrl']
        )
        # Get URL for SQS queue
        queueURL = sqs.get_queue_url(QueueName='OutputQueue')
        try:
            response = sqs.receive_message(
                QueueUrl=queueURL['QueueUrl'],
                MaxNumberOfMessages=1,
                VisibilityTimeout=10,
                WaitTimeSeconds=20,
                ReceiveRequestAttemptId='string'
            )
            message = response['Messages'][0]
            receipt_handle = message['ReceiptHandle']

            # Delete received message from queue
            sqs.delete_message(
                QueueUrl=queueURL['QueueUrl'],
                ReceiptHandle=receipt_handle
            )
            #print('Received and deleted message: %s' % message)
            successResponse = {
                "statusCode": 200,
                "body": json.dumps(message["Body"])
            }
            return successResponse
        except Exception as e:
            errorResponse = {
                "errorType": "Exception",
                "errorMessage": str(e)
            }
            return errorResponse
    except Exception as e:
        errorResponse = {
            "errorType": "Exception",
            "errorMessage": str(e)
        }
        return errorResponse
