import boto3

# Create SQS client
sqs = boto3.client('sqs')

# Get URL for SQS queue
queueURL = sqs.get_queue_url(QueueName='InputQueue')

print(queueURL['QueueUrl'])


# write to queue
